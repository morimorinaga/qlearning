# frozen_string_literal: true

class Teacher < ApplicationRecord
  belongs_to :user
  has_many :lectures, dependent: :destroy

  def self.is_logged_in?(user_id)
    return false if find_by(user_id: user_id).nil?

    true
  end
end
