class Chapter < ApplicationRecord
  belongs_to :lecture
  has_many :homework, dependent: :destroy
end
