class Submission < ApplicationRecord
  belongs_to :homework
  belongs_to :student
end
