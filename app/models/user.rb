# frozen_string_literal: true

class User < ApplicationRecord
  acts_as_authentic do |c|
    c.crypto_provider = Authlogic::CryptoProviders::BCrypt
  end

  # TODO: dependentの取り扱いを検討する
  has_one :teacher, dependent: :destroy
  has_one :student, dependent: :destroy
  accepts_nested_attributes_for :teacher
  accepts_nested_attributes_for :student

  validates :email,
            format: {
              with: /@/,
              message: 'should look like an email address.'
            },
            length: { maximum: 100 },
            uniqueness: {
              case_sensitive: false,
              if: :will_save_change_to_email?
            }

  validates :password,
            confirmation: { if: :require_password? },
            length: {
              minimum: 8,
              if: :require_password?
            }

  validates :password_confirmation,
            length: {
              minimum: 8,
              if: :require_password?
            }
end
