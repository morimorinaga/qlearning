class Homework < ApplicationRecord
  belongs_to :chapter
  has_many :submissions, dependent: :destroy
  has_many :questions, dependent: :destroy
end
