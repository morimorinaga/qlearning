# frozen_string_literal: true

class UserSession < Authlogic::Session::Base
  secure Rails.env.production?
  httponly true
end
