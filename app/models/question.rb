class Question < ApplicationRecord
  belongs_to :homework
  belongs_to :student
  has_one :question_point, dependent: :destroy
  has_many :answers, dependent: :destroy
end
