# frozen_string_literal: true

class Student < ApplicationRecord
  belongs_to :user
  has_many :lecture_students, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :submissions, dependent: :destroy
  has_many :answers, dependent: :destroy

  def self.is_logged_in?(user_id)
    return false if find_by(user_id: user_id).nil?

    true
  end
end
