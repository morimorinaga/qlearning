class Answer < ApplicationRecord
  belongs_to :question
  has_one :answer_point, dependent: :destroy
  # 本当は teacher or student と紐づくが、双方にNotNull制約を入れていないため、ちゃんとバリデーションかけてあげること！
end
