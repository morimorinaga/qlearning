class AnswerPointsController < ApplicationController
    
  def index
    @answerpoint = AnswerPoint.all
  end

  def show
    @answerpoint = AnswerPoint.find(params[:id])
  end

  def new
    @answerpoint = AnswerPoint.new
  end

  def create
    answer_id = params[:answer_point][:answer_id]
    point = params[:answer_point][:point]
    @answerpoint =AnswerPoint.create(answer_id: answer_id, point: point)
    if @answerpoint.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/answer_points'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @answerpoint = AnswerPoint.find(params[:id])
  end

  #update->編集のアップデート
  def update
    answer_id = params[:answer_point][:answer_id]
    point = params[:answer_point][:point]
    answerpoint = AnswerPoint.find(params[:id])
    answerpoint.update(answer_id: answer_id, point: point)

      redirect_to '/answer_points'

  end

  def destroy
    answerpoint = AnswerPoint.find(params[:id])
    answerpoint.destroy
    #一覧ページへリダイレクト
    redirect_to '/answer_points'
  end
end
