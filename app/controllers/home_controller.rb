# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    # トップ画面の表示用なので何もしません。
  end
end
