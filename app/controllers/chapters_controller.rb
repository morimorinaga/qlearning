class ChaptersController < ApplicationController
    
  def index
    @chapter = Chapter.all.order(:id)
    @teacher = Teacher.find_by_user_id(current_user.id)
  end

  def show
    @chapter = Chapter.find(params[:id])
    @lecture = Lecture.find_by_id(@chapter.lecture_id)
    @teacher = Teacher.find_by_user_id(current_user.id)
    @student = Student.find_by_user_id(current_user.id)
    @homework = Homework.where(chapter_id: params[:id])
  end

  def new
    @chapter = Chapter.new
    @lecture_id = params[:lecture_id]
  end

  def create
    lecture_id = params[:chapter][:lecture_id]
    chapter_no = params[:chapter][:chapter_no]
    title = params[:chapter][:title]
    note = params[:chapter][:note]
    first_chapter_no = params[:first_chapter_no]
    first_chapter_no.to_i.upto(chapter_no.to_i) do |num|
      @chapter =Chapter.create(lecture_id: lecture_id, chapter_no: num, title: title, note: note)
    end
        if @chapter.save
           #saveが完了したら、一覧ページへリダイレクト
          redirect_to '/chapters'
        else
          #saveを失敗すると新規作成ページへ
           render 'new'
        end
  end

  def edit
  @chapter = Chapter.find(params[:id])
  end

  #update->編集のアップデート
  def update
    lecture_id = params[:chapter][:lecture_id]
    chapter_no = params[:chapter][:chapter_no]
    title = params[:chapter][:title]
    note = params[:chapter][:note]
    chapter = Chapter.find(params[:id])
    chapter.update(lecture_id: lecture_id, chapter_no: chapter_no, title: title, note: note)

      redirect_to '/chapters'

  end

  def destroy
    chapter = Chapter.find(params[:id])
    chapter.destroy
    #一覧ページへリダイレクト
    redirect_to '/chapters'
  end
  
 
end
