# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :require_login, only: %i[index show edit edit_teacher update destroy]

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
    @user.build_student
  end

  def new_teacher
    @user = User.new
    @user.build_teacher
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to '/users'
    else
      render :new
    end
  end

  def edit
    # とりあえず該当ユーザーじゃないやつがやろうとしたら弾く。
    redirect_to "/403.html" if params[:id] != current_user.id.to_s
    redirect_to "/users/edit/#{params[:id]}/teacher" if Teacher.is_logged_in?(current_user.id)

    @user = User.find(params[:id])
  end

  def edit_teacher
    # とりあえず該当ユーザーじゃないやつがやろうとしたら弾く。
    redirect_to "/403.html" if params[:id] != current_user.id.to_s
    redirect_to "/users/edit/#{params[:id]}" if Student.is_logged_in?(current_user.id)

    @user = User.find(params[:id])
  end

  def update
    # とりあえず該当ユーザーじゃないやつがやろうとしたら弾く。
    redirect_to "/403.html" if params[:id] != current_user.id.to_s

    @user = User.find(params[:id])
    @user.update(user_params)
    redirect_to "/users/#{params[:id]}"
  end

  def destroy
    # とりあえず該当ユーザーじゃないやつがやろうとしたら弾く。
    redirect_to "/403.html" if params[:id] != current_user.id.to_s

    user = User.find(params[:id])
    user.destroy
    redirect_to '/users'
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, teacher_attributes: %i[id user_id name note], student_attributes: %i[id user_id name])
  end
end
