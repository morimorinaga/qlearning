class HomeworksController < ApplicationController

  def index
    @homework = Homework.all
  end

  def show
    @homework = Homework.find(params[:id])
    @teacher = Teacher.find_by_user_id(current_user.id)
    @student = Student.find_by_user_id(current_user.id)
    @submission = Submission.where(homework_id: params[:id])
    if @student.present?
     submission_student = Submission.where(homework_id: params[:id])
     @student_submission = submission_student.where(student_id: @student.id)
    end
  end

  def new
    @homework = Homework.new
    @chapterid = params[:chapter_id]
  end

  def create
    chapter_id = params[:homework][:chapter_id]
    title = params[:homework][:title]
    note = params[:homework][:note]
    closing_at = params[:homework][:closing_at]
    @homework =Homework.create(chapter_id: chapter_id, title: title, note: note, closing_at: closing_at)
    if @homework.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/homeworks'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @homework = Homework.find(params[:id])
  end

  #update->編集のアップデート
  def update
    chapter_id = params[:homework][:chapter_id]
    title = params[:homework][:title]
    note = params[:homework][:note]
    closing_at = params[:homework][:closing_at]
    homework = Homework.find(params[:id])
    homework.update(chapter_id: chapter_id, title: title, note: note, closing_at: closing_at)

      redirect_to '/homeworks'

  end

  def destroy
    homework = Homework.find(params[:id])
    homework.destroy
    #一覧ページへリダイレクト
    redirect_to '/homeworks'
  end
  
 
end
