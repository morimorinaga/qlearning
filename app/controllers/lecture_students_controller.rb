class LectureStudentsController < ApplicationController

  def index
    @lecturestudent = LectureStudent.all
  end

  def show
    @lecturestudent = LectureStudent.find(params[:id])
  end

  def new
    @lecturestudent = LectureStudent.new
  end

  def create
    lecture_id = params[:lecture_student][:lecture_id]
    student_id = params[:lecture_student][:student_id]
    @lecturestudent =LectureStudent.create(lecture_id: lecture_id, student_id: student_id)
    if @lecturestudent.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/lecture_students'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @lecturestudent = LectureStudent.find(params[:id])
  end

  #update->編集のアップデート
  def update
    lecture_id = params[:lecture_student][:lecture_id]
    student_id = params[:lecture_student][:student_id]
    lecturestudent = LectureStudent.find(params[:id])
    lecturestudent.update(lecture_id: lecture_id, student_id: student_id)

      redirect_to '/lecture_students'

  end

  def destroy
    lecturestudent = LectureStudent.find(params[:id])
    lecturestudent.destroy
    #一覧ページへリダイレクト
    redirect_to '/lecture_students'
  end
  
  



end
