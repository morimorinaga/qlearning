class AnswersController < ApplicationController

  def index
    @answer = Answer.all
  end

  def show
    @answer = Answer.find(params[:id])
  end

  def new
    @answer = Answer.new
    @question_id = params[:question_id]
  end

  def create
    question_id = params[:answer][:question_id]
    student_id = params[:answer][:student_id]
    body = params[:answer][:body]
    @answer =Answer.create(question_id: question_id, student_id: student_id, body: body)
    if @answer.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/answers'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @answer = Answer.find(params[:id])
  end

  #update->編集のアップデート
  def update
    question_id = params[:answer][:question_id]
    student_id = params[:answer][:student_id]
    body = params[:answer][:body]
    answer = Answer.find(params[:id])
    answer.update(question_id: question_id, student_id: student_id, body: body)

      redirect_to '/answers'

  end

  def destroy
    answer = Answer.find(params[:id])
    answer.destroy
    #一覧ページへリダイレクト
    redirect_to '/answers'
  end
  
 
    
end
