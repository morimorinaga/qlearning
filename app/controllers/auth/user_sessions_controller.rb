# frozen_string_literal: true

module Auth
  class UserSessionsController < ApplicationController
    def new
      @user_session = UserSession.new
    end

    def create
      @user_session = UserSession.new(user_session_params.to_h)
      if @user_session.save
        target_user = User.find_by(email: user_session_params['email'])
        target_user.update(last_login_at: Time.current)
        redirect_to '/'
      else
        render :new
      end
    end

    def destroy
      current_user_session.destroy
      redirect_to '/'
    end

    private

    def user_session_params
      params.require(:user_session).permit(:email, :password)
    end
  end
end
