class QuestionPointsController < ApplicationController
 
  def index
    @questionpoint = QuestionPoint.all
  end

  def show
    @questionpoint = QuestionPoint.find(params[:id])
  end

  def new
    @questionpoint = QuestionPoint.new
  end

  def create
    question_id = params[:question_point][:question_id]
    point = params[:question_point][:point]
    @questionpoint =QuestionPoint.create(question_id: question_id, point: point)
    if @questionpoint.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/question_points'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @questionpoint = QuestionPoint.find(params[:id])
  end

  #update->編集のアップデート
  def update
    question_id = params[:question_point][:question_id]
    point = params[:question_point][:point]
    questionpoint = QuestionPoint.find(params[:id])
    questionpoint.update(question_id: question_id, point: point)

      redirect_to '/question_points'

  end

  def destroy
    questionpoint = QuestionPoint.find(params[:id])
    questionpoint.destroy
    #一覧ページへリダイレクト
    redirect_to '/question_points'
  end
    
end
