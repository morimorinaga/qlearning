class LecturesController < ApplicationController

  def index
    @lecture = Lecture.all.order(:id)
    @teacher = Teacher.find_by_user_id(current_user.id)
  end

  def show
    @lecture = Lecture.find(params[:id])
    @teacher = Teacher.find_by_user_id(current_user.id)
    @chapter = Chapter.where(lecture_id: params[:id])
  end

  def new
    @lecture = Lecture.new
  end

  def create
    teacher = Teacher.find_by_user_id(current_user.id)
    # teacher_id = params[:lecture][:teacher_id]
    teacher_id = teacher.id
    name = params[:lecture][:name]
    note = params[:lecture][:note]
    question_point = params[:lecture][:question_point]
    answer_point = params[:lecture][:answer_point]
    @lecture =Lecture.create(teacher_id: teacher_id, name: name, note: note, question_point: question_point, answer_point: answer_point)
    if @lecture.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/lectures'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @lecture = Lecture.find(params[:id])
  end

  #update->編集のアップデート
  def update
    teacher_id = params[:lecture][:teacher_id]
    name = params[:lecture][:name]
    note = params[:lecture][:note]
    question_point = params[:lecture][:question_point]
    answer_point = params[:lecture][:answer_point]
    lecture = Lecture.find(params[:id])
    lecture.update(teacher_id: teacher_id, name: name, note: note, question_point: question_point, answer_point: answer_point)

      redirect_to '/lectures'

  end

  def destroy
    lecture = Lecture.find(params[:id])
    lecture.destroy
    #一覧ページへリダイレクト
    redirect_to '/lectures'
  end
  
  
end
