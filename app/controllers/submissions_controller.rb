class SubmissionsController < ApplicationController


  def index
    @submission = Submission.all
    @teacher = Teacher.find_by_user_id(current_user.id)
  end

  def show
    @submission = Submission.find(params[:id])
    @teacher = Teacher.find_by_user_id(current_user.id)
  end

  def new
    @submission = Submission.new
    @teacher = Teacher.find_by_user_id(current_user.id)
    @student = Student.find_by_user_id(current_user.id)
    @homework_id = params[:homework_id]
  end

  def create
    student = Student.find_by_user_id(current_user.id)
    homework_id = params[:submission][:homework_id]
    student_id = student.id
    note = params[:submission][:note]
    file_uri = params[:submission][:file_uri]
    score = params[:submission][:score]
    comment = params[:submission][:comment]
    @submission =Submission.create(homework_id: homework_id, student_id: student_id, note: note, file_uri: file_uri, score: score, comment: comment)
    if @submission.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/submissions'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @submission = Submission.find(params[:id])
  @teacher = Teacher.find_by_user_id(current_user.id)
  @student = Student.find_by_user_id(current_user.id)
  end

  #update->編集のアップデート
  def update
    homework_id = params[:submission][:homework_id]
    student_id = params[:submission][:student_id]
    note = params[:submission][:note]
    file_uri = params[:submission][:file_uri]
    score = params[:submission][:score]
    comment = params[:submission][:comment]
    submission = Submission.find(params[:id])
    submission.update(homework_id: homework_id, student_id: student_id, note: note, file_uri: file_uri, score: score, comment: comment)

      redirect_to '/submissions'

  end

  def destroy
    submission = Submission.find(params[:id])
    submission.destroy
    #一覧ページへリダイレクト
    redirect_to '/submissions'
  end
  
  
    
end
