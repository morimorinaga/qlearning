class QuestionsController < ApplicationController
  def index
    @question = Question.all
  end

  def show
    @question = Question.find(params[:id])
    @answers = Answer.all
    @teacher = Teacher.find_by_user_id(current_user.id)
    @student = Student.find_by_user_id(current_user.id)
  end

  def new
    @question = Question.new
  end

  def create
    homework_id = params[:question][:homework_id]
    student_id = params[:question][:student_id]
    title = params[:question][:title]
    body = params[:question][:body]
    file_uri = params[:question][:file_uri]
    like_count = params[:question][:like_count]
    solved_at = params[:question][:solved_at]
    @question =Question.create(homework_id: homework_id, student_id: student_id, title: title, body: body,file_uri: file_uri,like_count: like_count,solved_at: solved_at)
    if @question.save
      #saveが完了したら、一覧ページへリダイレクト
      redirect_to '/questions'
    else
      #saveを失敗すると新規作成ページへ
      render 'new'
    end
  end

  def edit
  @question = Question.find(params[:id])
  end

  #update->編集のアップデート
  def update
    homework_id = params[:question][:homework_id]
    student_id = params[:question][:student_id]
    title = params[:question][:title]
    body = params[:question][:body]
    file_uri = params[:question][:file_uri]
    like_count = params[:question][:like_count]
    solved_at = params[:question][:solved_at]
    question = Question.find(params[:id])
    question.update(homework_id: homework_id, student_id: student_id, title: title, body: body,file_uri: file_uri,like_count: like_count,solved_at: solved_at)

      redirect_to '/questions'

  end

  def destroy
    question = Question.find(params[:id])
    question.destroy
    #一覧ページへリダイレクト
    redirect_to '/questions'
  end

end
