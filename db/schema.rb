# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_18_063606) do

  create_table "answer_points", force: :cascade do |t|
    t.integer "answer_id"
    t.integer "point"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["answer_id"], name: "index_answer_points_on_answer_id"
  end

  create_table "answers", force: :cascade do |t|
    t.integer "question_id"
    t.integer "student_id"
    t.integer "teacher_id"
    t.string "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
  end

  create_table "chapters", force: :cascade do |t|
    t.integer "lecture_id"
    t.integer "chapter_no", null: false
    t.string "title", null: false
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_chapters_on_lecture_id"
  end

  create_table "homeworks", force: :cascade do |t|
    t.integer "chapter_id"
    t.string "title", null: false
    t.string "note"
    t.date "closing_at", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["chapter_id"], name: "index_homeworks_on_chapter_id"
  end

  create_table "lecture_students", force: :cascade do |t|
    t.integer "lecture_id"
    t.integer "student_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lecture_id"], name: "index_lecture_students_on_lecture_id"
    t.index ["student_id"], name: "index_lecture_students_on_student_id"
  end

  create_table "lectures", force: :cascade do |t|
    t.integer "teacher_id"
    t.string "name", null: false
    t.text "note"
    t.integer "question_point"
    t.integer "answer_point"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["teacher_id"], name: "index_lectures_on_teacher_id"
  end

  create_table "question_points", force: :cascade do |t|
    t.integer "question_id"
    t.integer "point", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_question_points_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.integer "homework_id"
    t.integer "student_id", null: false
    t.string "title", null: false
    t.string "body", null: false
    t.string "file_uri"
    t.integer "like_count"
    t.date "solved_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["homework_id"], name: "index_questions_on_homework_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "user_id"
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_students_on_user_id"
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "homework_id"
    t.integer "student_id", null: false
    t.string "note"
    t.string "file_uri", null: false
    t.integer "score"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["homework_id"], name: "index_submissions_on_homework_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.integer "user_id"
    t.string "name", null: false
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_teachers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "crypted_password"
    t.string "password_salt"
    t.string "persistence_token"
    t.integer "login_count", default: 0, null: false
    t.integer "failed_login_count", default: 0, null: false
    t.string "current_login_ip"
    t.string "last_login_ip"
    t.datetime "last_request_at"
    t.datetime "last_login_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
