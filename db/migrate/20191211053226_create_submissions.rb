class CreateSubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :submissions do |t|
      t.belongs_to :homework
      t.integer :student_id, null: false
      t.string :note
      t.string :file_uri, null: false
      t.integer :score
      t.text :comment

      t.timestamps
    end
  end
end
