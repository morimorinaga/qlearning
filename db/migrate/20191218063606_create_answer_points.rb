class CreateAnswerPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :answer_points do |t|
      t.belongs_to :answer
      t.integer :point

      t.timestamps
    end
  end
end
