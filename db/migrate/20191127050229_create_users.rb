# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.string :crypted_password
      t.string :password_salt
      t.string :persistence_token
      t.integer  :login_count, default: 0, null: false
      t.integer  :failed_login_count, default: 0, null: false
      t.string   :current_login_ip
      t.string   :last_login_ip
      t.datetime :last_request_at
      t.datetime :last_login_at

      t.timestamps
    end
  end
end
