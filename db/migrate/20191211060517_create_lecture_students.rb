class CreateLectureStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :lecture_students do |t|
      t.belongs_to :lecture
      t.belongs_to :student

      t.timestamps
    end
  end
end
