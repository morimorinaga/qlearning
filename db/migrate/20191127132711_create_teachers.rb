# frozen_string_literal: true

class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.belongs_to :user
      t.string :name, null: false
      t.text :note

      t.timestamps
    end
  end
end
