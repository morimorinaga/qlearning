class CreateLectures < ActiveRecord::Migration[6.0]
  def change
    create_table :lectures do |t|
      t.belongs_to :teacher
      t.string :name, null:false
      t.text :note
      t.integer :question_point
      t.integer :answer_point

      t.timestamps
    end
  end
end
