class CreateHomeworks < ActiveRecord::Migration[6.0]
  def change
    create_table :homeworks do |t|
      t.belongs_to :chapter
      t.string :title, null: false
      t.string :note
      t.date :closing_at, null: false

      t.timestamps
    end
  end
end
