class CreateAnswers < ActiveRecord::Migration[6.0]
  def change
    create_table :answers do |t|
      t.belongs_to :question
      t.integer :student_id
      t.integer :teacher_id
      t.string :body

      t.timestamps
    end
  end
end
