class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.belongs_to :homework
      t.integer :student_id, null: false
      t.string :title, null: false
      t.string :body, null: false
      t.string :file_uri
      t.integer :like_count
      t.date :solved_at

      t.timestamps
    end
  end
end
