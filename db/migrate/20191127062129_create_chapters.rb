class CreateChapters < ActiveRecord::Migration[6.0]
  def change
    create_table :chapters do |t|
      t.belongs_to :lecture
      t.integer :chapter_no, null: false
      t.string :title, null: false
      t.text :note

      t.timestamps
    end
  end
end
