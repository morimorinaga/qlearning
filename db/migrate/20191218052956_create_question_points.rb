class CreateQuestionPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :question_points do |t|
      t.belongs_to :question
      t.integer :point, null: false

      t.timestamps
    end
  end
end
