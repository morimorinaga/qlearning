FactoryBot.define do
  factory :question do
    homework_id { 1 }
    student_id { 1 }
    title { "MyString" }
    body { "MyString" }
    file_uri { "MyString" }
    like_count { 1 }
    solved_at { "2019-12-18" }
  end
end
