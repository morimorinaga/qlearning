FactoryBot.define do
  factory :submission do
    homework_id { 1 }
    student_id { 1 }
    note { "MyString" }
    file_uri { "MyString" }
    score { 1 }
    comment { "MyText" }
  end
end
