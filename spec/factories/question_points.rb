FactoryBot.define do
  factory :question_point do
    question_id { 1 }
    point { 1 }
  end
end
