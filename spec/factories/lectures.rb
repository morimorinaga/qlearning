FactoryBot.define do
  factory :lecture do
    teacher_id { 1 }
    name { "MyString" }
    note { "MyText" }
    question_point { 1 }
    answer_point { 1 }
  end
end
