# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'hoge@example.com' }
    crypted_password { 'cryptedpass' }
    password_salt { 'MyString' }
    persistence_token { 'MyString' }
    login_count { 1 }
    failed_login_count { 1 }
    last_request_at { '2019-11-27 14:02:29' }
    last_login_at { '2019-11-27 14:02:29' }
    current_login_ip { '111.111.111.111' }
    last_login_ip { '222.222.222.222' }
  end
end
