FactoryBot.define do
  factory :chapter do
    lecture_id { 1 }
    chapter_no { 1 }
    title { "MyString" }
    note { "MyString" }
  end
end
