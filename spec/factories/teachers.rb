# frozen_string_literal: true

FactoryBot.define do
  factory :teacher do
    user_id { 1 }
    name { 'Professor Tanaka' }
    note { "Professor's self introduction" }
  end
end
