# frozen_string_literal: true

FactoryBot.define do
  factory :student do
    user_id { 1 }
    name { 'Seito Onamae' }
  end
end
