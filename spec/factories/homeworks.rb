FactoryBot.define do
  factory :homework do
    chapter_id { 1 }
    title { "MyString" }
    note { "MyString" }
    closing_at { "2019-12-11" }
  end
end
