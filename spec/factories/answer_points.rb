FactoryBot.define do
  factory :answer_point do
    answer_id { 1 }
    point { 1 }
  end
end
