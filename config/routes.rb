# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'
  resources :home, only: :index
  resources :users
  get 'users/new/teacher' => 'users#new_teacher'
  get 'users/edit/:id/teacher' => 'users#edit_teacher'

  # Authentication
  namespace :auth do
    post '' => 'user_sessions#create'
    get 'login' => 'user_sessions#new'
    delete 'logout' => 'user_sessions#destroy'
  end

  resources :chapters
  resources :lectures
  resources :homeworks
  resources :submissions
  resources :lecture_students
  resources :questions
  resources :question_points
  resources :answers
  resources :answer_points
end
