# Qlearning

### 環境構築方法

1. git clone する
```
git clone https://moririn772@bitbucket.org/morimorinaga/qlearning.git
```

2. bundle install する
```
cd qlearning
bundle install
```

3. データベースの準備をする
```
bin/rails db:migrate
```

4. 起動する
```
bin/rails s -b 0.0.0.0
```

### バージョン
- Ruby: 2.6.5  
- Rails: 6.0.1